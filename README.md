# Proyecto_Grupal_Web_Backend
# Proyecto centrado en la funcionalidad del backend con node js
# Renderización desde el servidor con plantillas realizadas en pug 

--------------------------------------------------------------------------

# tecnologías utilizadas
# Express js
# Node js
# JavaScript
# Pug 
# Docker 
# Mongodb 
# Angular

---------------------------------------------------------------------------

# Contribuidores del proyecto
# @jamicedcev
# @Karlimeli

# Evaluador
# @joancema

---------------------------------------------------------------------------

# Objetivo del proyecto

# Realizar el proceso logico del backen por medio de node js, con una estructura
# escalable a nuevos modulos, con interfaces de prueba